package Ro.Orange;

public class GameLauncher {

    public static void main(String[] args) {
        int guess;
        int g1;
        int g2;
        int g3;

		GuessGame G1= new GuessGame();
		guess=G1.getNrToGuess();
        Player Alex = new Player();
		g1=Alex.getAlexNumber();
        Player Bus = new Player();
        g2=Bus.getBusNumber();
        Player AlexBus = new Player();
        g3=AlexBus.getAlexBusNumber();

        System.out.println("I'm thinking of a number between 0 and 10. It's: "+guess);
        System.out.println("Alex: i'm guessing "+g1);
        System.out.println("Bus: i'm guessing "+g2);
        System.out.println("AlexBus: i'm guessing "+g3);
        System.out.println("Alex guessed: "+g1);
        System.out.println("Bus guessed: "+g2);
        System.out.println("AlexBus guessed: "+g3);
        if (guess==g1) {
            System.out.println("We have a winner!");
            System.out.println("Alex got it right? Yes!");
            System.out.println("Bus got it right? No!");
            System.out.println("AlexBus got it right? No!");
            System.out.println("The game is over!");
        } else if (guess==g2) {
            System.out.println("We have a winner!");
            System.out.println("Alex got it right? No!");
            System.out.println("Bus got it right? Yes!");
            System.out.println("AlexBus got it right? No!");
            System.out.println("The game is over!");
        } else if (guess==g3) {
            System.out.println("We have a winner!");
            System.out.println("Alex got it right? No!");
            System.out.println("Bus got it right? No!");
            System.out.println("AlexBus got it right? Yes!");
            System.out.println("The game is over!");
        } else {System.out.println("Players will have to try again!");

        }
    }
}
