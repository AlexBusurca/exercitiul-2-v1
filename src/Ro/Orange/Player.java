package Ro.Orange;

import java.util.Random;

public class Player {

    public Player() {
    }

    Random randA = new Random();
    private int AlexNumber = randA.nextInt(11);
    Random randB = new Random();
    private int BusNumber = randB.nextInt(11);
    Random randAB = new Random();
    private int AlexBusNumber = randAB.nextInt(11);

    public int getAlexNumber() {
        return this.AlexNumber;
    }

    public int getBusNumber() {
        return this.BusNumber;
    }

    public int getAlexBusNumber() {
        return this.AlexBusNumber;
    }

}
